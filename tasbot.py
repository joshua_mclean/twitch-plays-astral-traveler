import pyautogui
import time

from timeit import default_timer as timer

__countdown = 5
__time_start = 0


def delay_start(func):
    remaining = __countdown
    while True:
        if remaining > 0:
            time.sleep(1)
            remaining -= 1
            print('Starting in', remaining, 'seconds')
        else:
            func()
            break


def hold_input(key, sec):
    pyautogui.keyDown(key)
    time.sleep(sec)
    pyautogui.keyUp(key)


def press_input(key):
    pyautogui.press(key)
    time.sleep(0.2)


def set_countdown(val):
    global __countdown
    try:
        __countdown = int(val)
    except TypeError:
        print('Tried to set countdown to non-int')
        return


def get_elapsed_time():
    return timer() - __time_start


def start_timer():
    global __time_start
    __time_start = timer()


def print_timestamp():
    print(get_elapsed_time(), end=' ')
