import random

debug = False


def roll(user, args):
    a = random.randint(1, 4)
    b = random.choice([4, 6, 8, 10, 12, 20])
    c = random.randint(1, 8)
    d = str(a) + 'd' + str(b) + '+' + str(c)
    instruction = 'You must provide dice to roll, e.g. ' + d + '.'
    if args is None:
        return instruction

    if 'joint' in args:
        return 'Sorry, that is not something we roll here.'

    if 'help' in args or len(args) != 1 or 'd' not in args[0]:
        return instruction

    try:
        d_split = args[0].split('d')
        if len(d_split) > 2:
            return instruction
        dice, faces = d_split
        if not dice.isdigit() or not faces.isdigit():
            return instruction
        bonus = 0
        if '+' in faces:
            faces, bonus = faces.split('+')
            bonus = int(bonus)
        elif '-' in faces:
            faces, bonus = faces.split('-')
            bonus = -int(bonus)
        dice = int(dice)
        faces = int(faces)
        if dice > 100000:
            return 'Sorry, you can only roll up to 100,000 dice. Which, by the way, is plenty.'

        msg = 'rolls ' + str(dice) + 'd' + str(faces)

        if bonus > 0:
            msg += '+' + str(bonus)
        elif bonus < 0:
            msg += str(bonus)

        msg += '. Result: '

        result = 0
        for i in range(0, dice):
            r = random.randint(1, faces)
            result += r + bonus
            if debug:
                msg += str(r)
                if bonus > 0:
                    msg += '+' + str(bonus)
                elif bonus < 0:
                    msg += str(bonus)
                msg += ' + '

        if debug:
            msg = msg[:-3]

        if result < 1:
            result = 1
        msg += ' = ' + str(result)

        print(msg)
        return msg
    except Exception as e:
        print('Error:', e)
        return 'Sorry, there was an error processing your roll. ' + instruction
