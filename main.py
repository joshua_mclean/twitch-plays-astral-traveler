import time

import command
import tasrunner
import twitch

last_help = 0


def show_help(user, args):
    global last_help
    min = 200
    if time.time() - last_help < min:
        next_help = min - int(time.time() - last_help)
        msg = f'Sorry, I cannot help for another {next_help} seconds.'
    else:
        last_help = time.time()
        msg = '(1/4) Type commands to control the Astral Traveler! (not cast-sensitive)\n' \
              + '(2/4) Commands marked with [x] have optional time given in 1/100th of a second\n'\
              + '(3/4) Ship controls: (l)eft [time], (r)ight [time], (a)scend [time], (d)escend [time], '\
                  + '(j)ump*, (s)hoot [times], (p)hase [delay], (w)ait [time]\n'\
              + '(4/4) Extra controls: (b)ack - pause game in real-time mode)'
              #+ 'UI controls: (e)nter, (b)ack, restart, (l)eft, (r)ight to navigate UI'
    twitch.bot.message(msg)


def main():
    tasrunner.turn_based = True
    tasrunner.panic()
    print('(that\'s supposed to happen)')

    tasrunner.add_admin('MrJoshuaMcLean')
    tasrunner.add_admin('Seciden')

    twitch.init()

    # delay to allow time for clicking the game
    if tasrunner.turn_based:
        print('*TURN BASED*\nMAKE SURE GAME IS UNPAUSED AND IN LEVEL')

    wait_time = 5
    for i in range(wait_time):
        print(f'Giving control to Twitch stream in {wait_time - i} seconds')
        time.sleep(1)

    twitch.bot.message_prefix = '[AT] '
    twitch.bot.message(f'Connected to Astral Traveler! Admins: {tasrunner.admin_list}')

    print('Use (addplayer) to add players, (addadmin) to add an admin, (kick) to remove players.')

    command.add('help', show_help)
    command.add('a', tasrunner.ascend, True)
    command.add('ascend', tasrunner.ascend, True)
    command.add('addplayer', tasrunner.add_player, True)
    command.add('b', tasrunner.back, True)
    command.add('back', tasrunner.back, True)
    command.add('d', tasrunner.descend, True)
    command.add('descend', tasrunner.descend, True)
    #command.add('e', tasrunner.enter, True)
    command.add('enter', tasrunner.enter, True)
    command.add('j', tasrunner.jump, True)
    command.add('jump', tasrunner.jump, True)
    #command.add('join', tasrunner.join, True)
    command.add('kick', tasrunner.kick, True)
    command.add('l', tasrunner.left, True)
    command.add('left', tasrunner.left, True)
    #command.add('leave', tasrunner.leave, True)
    command.add('p', tasrunner.phase, True)
    command.add('phase', tasrunner.phase, True)
    command.add('players', tasrunner.list_players, True)
    command.add('r', tasrunner.right, True)
    command.add('right', tasrunner.right, True)
    command.add('s', tasrunner.shoot, True)
    command.add('shoot', tasrunner.shoot, True)
    command.add('t', tasrunner.thrust_toggle, True)
    #command.add('thrust', tasrunner.thrust_toggle, True)
    command.add('w', tasrunner.wait, True)
    command.add('wait', tasrunner.wait, True)
    command.add('restart', tasrunner.restart, True)
    command.add('quit', tasrunner.end_game, True)
    command.allowed = tasrunner.allowed

    tasrunner.thrust_toggle(None, None)
    if tasrunner.turn_based:
        tasrunner.pause()
        tasrunner.restart('mrjoshuamclean', None)

    while True:
        twitch.update()
        time.sleep(1)


if __name__ == "__main__":
    main()
