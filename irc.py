from collections import namedtuple
from sys import exit as sysexit

import os
import socket
import time

BUFF_SIZE = 2048
USER_FILE = 'userlist.dat'

User = namedtuple('User', 'id name auth')


class IrcBot:
    # set true to see all commands sent to IRC and all messages received
    verbose = False

    message_prefix = ""

    channel = None
    username = None
    __auth = None

    __chat_list = []
    __user_list = []

    __connected = False
    __sock = None
    __logged_in = False
    __server = None
    __write = print

    def __init__(self, server, write_func, verbose=False):
        self.__server = server
        self.__write = write_func
        self.verbose = verbose
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__sock.setblocking(False)
        self.__connected = True
        self.__login_load()

    def close(self):
        self.__sock.close()
        self.__write('Connection closed')

    def channel_join(self, channel=None):
        if not self.__logged_in:
            self.__write('ERROR: Tried to join channel, but not logged in')
            return
        if channel is None:
            channel = self.username
        self.__send('JOIN #' + channel + '\r\n')
        self.channel = channel
        self.__write('Connected to channel ' + self.channel)
        self.read_all()

    def channel_leave(self):
        if not self.__logged_in:
            self.__write('ERROR: Tried to leave channel, but not logged in')
            return
        self.__send('PART #' + self.channel + '\r\n')
        self.channel = None
        self.read_all()

    def chat_pop(self):
        end = len(self.__chat_list) - 1
        if end < 0:
            return None, None
        user, chat = self.__chat_list[end]
        self.__chat_list = self.__chat_list[:-1]
        return user, chat

    def connect(self):
        try:
            self.__write('Connect to server...')
            self.__sock.settimeout(5)
            result = self.__sock.connect_ex(self.__server)
            self.__sock.settimeout(0.1)
            if result == 0:
                self.__write('success')
            else:
                self.__write('failure (' + result + ')')
        except Exception as e:
            self.__write('Error: %s' % e)

    def login_add(self, username, auth, write=True):
        i = len(self.__user_list)
        if username.find(' ') != -1 or auth.find(' ') != -1:
            self.__write('ERROR: Username and auth must not have spaces')
            return
        user = User(i, username, auth)
        if self.verbose:
            self.__write('Add user ' + str(i) + ' ' + user.name)
        self.__user_list.append(user)
        if write:
            self.__login_write()

    def login_do(self):
        if not self.__connected:
            self.__write('ERROR: Tried to login, but not connected to IRC server')
            return
        if self.__logged_in:
            self.__write('ERROR: Tried to login, but already logged in')
            return
        if self.username is None or self.__auth is None:
            self.__write('ERROR: Tried to login, but username and auth not set')
            return
        self.__send('pass ' + self.__auth, True)
        self.__send('nick ' + self.username)
        # TODO check response to see if we successfully logged in
        self.read_all()
        self.__write('Logged in as ' + self.username)
        self.__logged_in = True

    def login_list(self):
        self.__write('User list:')
        for user in self.__user_list:
            self.__write(user.id, user.name)

    def login_set(self, i):
        self.__auth = self.__user_list[i].auth
        self.username = self.__user_list[i].name
        if self.verbose:
            self.__write('# Login set to' + self.username)

    def login_set_recent(self):
        self.login_set(len(self.__user_list) - 1)

    def message(self, msg):
        if self.channel is None:
            self.__write('ERROR: Tried to post message, but not connected to a channel')
            return
        if '\n' in msg:
            msg_list = msg.split('\n')
            for m in msg_list:
                self.__send('privmsg #' + self.channel + ' :' + self.message_prefix + m)
                time.sleep(2)
        else:
            self.__send('privmsg #' + self.channel + ' :' + self.message_prefix + msg)

    def ping(self):
        self.__send('PONG :pingis')

    def print_received(self, msg):
        if not self.verbose:
            return
        self.__write('<< ' + msg.decode('UTF-8').replace('\\r\\n', '\n'))

    def read(self):
        try:
            msg = self.__sock.recv(BUFF_SIZE)
        except socket.error as e:
            err = e.args[0]
            if err == 'timed out':
                return None
            else:
                print(e)
                sysexit(1)
        self.print_received(msg)
        return msg.decode('UTF-8')

    def read_all(self):
        while True:
            try:
                msg = self.__sock.recv(BUFF_SIZE)
            except socket.error as e:
                err = e.args[0]
                if err == 'timed out':
                    return
                else:
                    print(e)
                    sysexit(1)
            self.print_received(msg)
            if len(msg) < BUFF_SIZE:
                return

    # TODO add timeout to be safe with potentially infinite loop
    def read_to(self, phrase):
        while True:
            msg = self.__sock.recv(BUFF_SIZE)
            self.print_received(msg)
            if msg.find(phrase):
                return

    def set_write_func(self, write):
        self.__write = write

    def update(self):
        msg = self.read()
        if msg is None:
            return
        msg = msg.lower()
        if msg.find('ping') != -1:
            self.ping()
        elif msg.find('privmsg') != -1:
            self.__process_chat(msg)
        else:
            print('unprocessed IRC message: "', msg, '"')

    def __login_load(self):
        if not os.path.isfile(USER_FILE):
            if self.verbose:
                self.__write('No user file exists')
            return
        with open(USER_FILE, 'r') as f:
            while True:
                line = f.readline()
                if line == '':
                    break
                words = line.rstrip().split(' ')
                self.login_add(words[0], words[1], False)

    def __login_write(self):
        with open(USER_FILE, 'w') as f:
            self.__write('Write user file')
            for user in self.__user_list:
                f.write(user.name + ' ' + user.auth)

    def __send(self, msg, encrypt=False):
        if self.verbose:
            if encrypt:
                self.__write('>> [encrypted message]')
            else:
                self.__write('>> ' + msg.rstrip())
        msg += '\r\n'
        self.__sock.send(msg.encode('UTF-8'))

    def __process_chat(self, msg):
        lines = msg.split('\n')
        for line in lines:
            if '!' not in line:
                continue
            user = line.split('!', 1)[0][1:]
            chat = line.split('privmsg', 1)[1].split(':', 1)[1]
            if self.verbose:
                self.__write('[' + user + '] : ' + chat)
            self.__chat_list.append((user, chat))
            if self.verbose:
                print('push: ', user, chat)
