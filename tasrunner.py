import pyautogui
import sys
import time

import tasbot

ENTER = 'enter'
BACK = 'escape'
THRUST = 'space'
LEFT = 'a'
RIGHT = 'd'
ASCEND = 'w'
DESCEND = 's'
SHOOT = 'j'
PHASE = 'l'
JUMP = 'k'
PAUSE = 'p'
RESTART = 'backspace'


admin_list = []
player_list = []
player_max = 3
turn_based = True

__nebula = 1
__sector = 1

__is_paused = False
__is_thrusting = False


def player_count():
    return len(player_list) - len(admin_list)


def join(user, args):
    if user in player_list:
        return f'You\'re already in the game! Use the (help) command to see what you can do. :)'
    if player_count() >= player_max:
        return f'Sorry, we have maximum players ({len(player_list)}/{player_max})'
    player_list.append(user)
    return f'has joined the game! {player_max - player_count()} slots remaining'


def list_players(user, args):
    return str(player_list)


def leave(user, args):
    if user not in player_list:
        return f'You can\'t leave - you\'re not in the game!'
    player_list.remove(user)
    if user in admin_list:
        admin_list.remove(user)
    return f'has left the game'


def kick(user, args):
    if user not in admin_list:
        return f'You cannot kick people, as you aren\'t an admin.'
    if len(args) == 0:
        return f'You must specify which user to kick.'
    if args[0] == user:
        return f'You cannot kick yourself!'
    leave(args[0], None)
    return f'kicked {args[0]} from the game'


def add_admin(user):
    player_list.append(user.lower())
    admin_list.append(user.lower())


def add_player(user, args):
    if user not in admin_list:
        return 'You cannot add people, as you aren\'t an admin.'
    if len(args) == 0:
        return 'You must provide a user to add.'
    print(f'args: {args}')
    join(args[0], None)
    return f'added {args[0]} to the game'


def allowed(user):
    return user in player_list


def ascend(user, args):
    if turn_based:
        pause()
    try:
        tasbot.press_input(ASCEND, hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(JUMP)
    if turn_based:
        unpause()


def back(user, args):
    if turn_based or user in admin_list:
        tasbot.press_input(BACK)


def enter(user, args):
    if user not in admin_list:
        return
    tasbot.press_input(ENTER)
    time.sleep(2)
    pause()


def jump(user, args):
    if turn_based:
        unpause()
    try:
        tasbot.hold_input(JUMP, hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(JUMP)
    if turn_based:
        pause()


def shoot(user, args):
    if turn_based:
        unpause()

    times = 0
    if len(args) > 0:
        times = int(args[0])
    if times > 20:
        times = 20

    for i in range(times):
        tasbot.press_input(SHOOT)

    if turn_based:
        pause()


def pause():
    print('game paused')
    global __is_paused
    if __is_paused:
        return
    tasbot.press_input(PAUSE)
    __is_paused = True


def unpause():
    print('game unpaused')
    global __is_paused
    if not __is_paused:
        return
    tasbot.press_input(PAUSE)
    __is_paused = False


def phase(user, args):
    if turn_based:
        unpause()
    try:
        time.sleep(hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(PHASE)
        pass
    tasbot.press_input(PHASE)
    if turn_based:
        pause()


def wait(user, args):
    if turn_based:
        unpause()
    try:
        time.sleep(hundredth_sec(args[0]))
    except TypeError:
        time.sleep(0.1)
    if turn_based:
        pause()


MAX_TIME_MS = 1000


def hundredth_sec(ms):
    ms = min(int(ms), MAX_TIME_MS)
    return int(ms / 10)


def left(user, args):
    if turn_based:
        unpause()
    try:
        tasbot.hold_input(LEFT, hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(LEFT)
        print('type error')
    if turn_based:
        pause()


def right(user, args):
    if turn_based:
        unpause()
    try:
        tasbot.hold_input(RIGHT, hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(RIGHT)
    if turn_based:
        pause()


def restart(user, args):
    if turn_based:
        unpause()
    tasbot.press_input(RESTART)
    time.sleep(1)
    if turn_based:
        pause()


def thrust_toggle(user, args):
    #if turn_based:
        #unpause()
    global __is_thrusting
    if __is_thrusting:
        pyautogui.keyUp(THRUST)
        __is_thrusting = False
    else:
        pyautogui.keyDown(THRUST)
        __is_thrusting = True
    #if turn_based:
        #pause()


def descend(user, args):
    if turn_based:
        unpause()
    try:
        tasbot.hold_input(RIGHT, hundredth_sec(args[0]))
    except TypeError:
        tasbot.press_input(DESCEND)
    if turn_based:
        pause()


def end_game(user, args):
    if user not in admin_list:
        print(user, 'tried to quit but not authorized')
        return
    panic()
    sys.exit()


def panic():
    print('PANIC')
    pyautogui.keyUp(THRUST)
    pyautogui.keyUp(LEFT)
    pyautogui.keyUp(RIGHT)
    pyautogui.keyUp(ASCEND)
    pyautogui.keyUp(DESCEND)
    pyautogui.keyUp(SHOOT)
    pyautogui.keyUp(PHASE)
    pyautogui.keyUp(JUMP)
    pyautogui.keyUp(PAUSE)


def set_nebula(val):
    global __nebula
    try:
        __nebula = int(val)
    except TypeError:
        print('Tried to set nebula to non-int')
        return


def set_sector(val):
    global __sector
    try:
        __sector = int(val)
    except TypeError:
        print('Tried to set sector to non-int')
        return
