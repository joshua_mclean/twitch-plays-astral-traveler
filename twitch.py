import command

from irc import IrcBot

TWITCH = ('irc.chat.twitch.tv', 6667)

bot = None
channel = 'mrjoshuamclean'
username = 1


def init(write_func=print, verbose=False):
    global bot
    bot = IrcBot(TWITCH, write_func, verbose)

    bot.connect()
    if username is None:
        bot.login_list()
        name = input('Enter name for new login or number to use loaded user: ')
        if name.isdigit():
            bot.login_set(int(name))
        else:
            auth = input('Enter auth: ')
            bot.login_add(name, auth)
            bot.login_set_recent()
    elif str(username).isdigit():
        bot.login_set(int(username))
    bot.login_do()

    if channel is None:
        bot.channel_join()
    else:
        bot.channel_join(channel)


def update():
    bot.update()
    while True:
        user, cmd = bot.chat_pop()
        if cmd is None:
            break
        command.handle(user, cmd)

