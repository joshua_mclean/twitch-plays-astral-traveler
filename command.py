import twitch

command_dict = {}


def add(cmd, func, exact_match=False):
    command_dict[cmd] = (func, exact_match)


def allowed(user):
    return True


def clear():
    command_dict.clear()


def remove(cmd):
    command_dict.pop(cmd)


# returns whether the command was successfully processed
def handle(user, cmd):
    print('command ::', user, ':', cmd)
    msg = '@' + user + ' '
    cmd = cmd.lower().rstrip()

    processed = False
    for key, val in command_dict.items():
        func = val[0]
        exact_match = val[1]
        if cmd.startswith(key):
            if ' ' in cmd:
                split = cmd.split(' ')
                action = split[0]
                args = split[1:]
            else:
                action = cmd
                args = None

            # abort if it's an exact-match command and first word isn't command
            if exact_match and action != key:
                print('non-exact match')
                continue

            if allowed(user):
                addendum = func(user, args)
                if addendum is not None:
                    processed = True
                    msg += addendum
            else:
                msg += f'An admin must add you to the game before you can enter commands'
                processed = True

    if processed:
        twitch.bot.message(msg)
    return processed


